//menu button
const button = document.getElementsByClassName("header__menu-btn")[0];
const menu = document.getElementsByClassName("header__menu-links")[0];
const link = document.querySelectorAll(".menu-link");

window.addEventListener("resize", function () {
  if (document.body.clientWidth >= 768) {
    menu.classList.remove("show");
    button.classList.remove("active");
  }
});

button.addEventListener("click", (ev) => {
  button.classList.toggle("active");
  menu.classList.toggle("show");
  link.forEach((el) => {
    el.classList.add("show");
  });
});

//loader
let mask = document.querySelector(`.mask`);
window.addEventListener(`load`, () => {
  mask.classList.add(`hide`);
  setTimeout(() => {
    mask.remove();
  }, 600);
});
