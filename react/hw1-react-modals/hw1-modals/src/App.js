import Button from './components/Button.js';
import Modal from './components/Modal.js';
import React, {Component} from 'react';
import './App.scss';



export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: null
    }
  }

  showFirstModal = () => {
    this.setState({
      status: "first-modal"
    })
  }

  showSecondModal = () => {
    this.setState({
      status: "second-modal"
    })
  }

  hideModal = () => {
    this.setState({
      status: null
    })
  }

  render() {
    return (
        <div>
          <div className={"wrapper"}>
            <Button backgroundColor="pink" text="Open first modal"  onClick={this.showFirstModal} className={"btn"}/>
            <Button backgroundColor="yellow" text="Open second modal" onClick={this.showSecondModal} className={"btn"}/>
          </div>



          {this.state.status === "first-modal" &&
          <Modal headerText={"Do you want to delete this file?"} closeButton={true}
                 text={"Once you delete this file, it won’t be possible to undo this action"} actions={[
            <Button backgroundColor="rgba(0,0,0,.3)" text="Ok" onClick={this.test} className="modal__buttons"/>,
            <Button backgroundColor="rgba(0,0,0,.3)" text="Cancel" onClick={this.test} className="modal__buttons"/>
          ]} status={this.hideModal}/>}


          {this.state.status === "second-modal" &&
          <Modal headerText={"Who`s better?"} closeButton={true}
                 text={"bmw or benz"} actions={[
            <Button backgroundColor="rgba(0,0,0,.3)" text="BMW" onClick={this.test} className="modal__buttons"/>,
            <Button backgroundColor="rgba(0,0,0,.3)" text="Mercedes-Benz" onClick={this.test} className="modal__buttons"/>
          ]} status={this.hideModal}/>}

        </div>)
  }
}
