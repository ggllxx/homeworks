import React, {Component} from 'react';

class Button extends Component {

    render() {
        const {backgroundColor, text, onClick, className} = this.props
        return (
            <div>
                <button className={className} style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
            </div>
        );
    }
}

Button.defaultProps = {
    className: ""
}

export default Button;
