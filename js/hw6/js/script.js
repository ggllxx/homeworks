"use strict";

function filterBy(arr, type) {
  let newArr = [];

  if (type === null) {
    newArr = arr.filter((element) => element !== null);
  } else {
    newArr = arr.filter((element) => typeof element !== typeof type);
  }
  return newArr;
}

console.log(
  filterBy(["hello", "world", 23, "23", 777, undefined, null], "string")
);
