"use strict";
const stopBtn = document.getElementById("stopBtn");
const startBtn = document.getElementById("startBtn");
const images = ["1.jpg", "2.jpg", "3.JPG", "4.png"];
let index = 0;
let timer;
let countTime;
let i = 10;
showImages();

function showImages() {
  if (index === images.length) index = 0;
  let atr = (document.getElementById(
    "Image"
  ).src = `./Images/${images[index]}`);
  index++;
}
timer = setInterval(showImages, 10000);

showTime();
function showTime() {
  document.getElementById("timerOut").innerHTML = i;
  i--;
  if (i === 0) i = 10;
  countTime = setTimeout(showTime, 1000);
}

stopBtn.addEventListener("click", function () {
  document.getElementById("timerOut").style.display = "none";
  document.getElementById("timerOut").innerHTML = "10";
  clearInterval(timer);
  clearInterval(countTime);
  startBtn.classList.remove("hidden");
});

startBtn.addEventListener("click", function () {
  i = 9;
  document.getElementById("timerOut").style.display = "block";
  timer = setInterval(showImages, 10000);
  countTime = setTimeout(showTime, 1000);
  startBtn.classList.add("hidden");
});
