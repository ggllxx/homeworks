"use strict";
const buttons = document.querySelectorAll(".btn");

document.addEventListener("keydown", function (event) {
  buttons.forEach((item) => {
    if (event.code.slice(3, 5) === item.getAttribute("id")) {
      item.classList.toggle("active");
    } else if (event.code === item.getAttribute("id")) {
      item.classList.toggle("active");
    } else {
      item.classList.remove("active");
    }
  });
});
