"use strict";
const input = document.getElementById("input");
const priceSpan = document.getElementsByClassName("price-span")[0];
const priceBox = document.getElementsByClassName("price-box")[0];
const deleteButton = document.getElementById("deleteButton");

input.onfocus = function () {
  this.style.borderColor = "green";
};

input.onblur = function () {
  if (parseInt(input.value) <= 0 || input.value === "") {
    this.value = "";
    input.style.color = "red";
    this.style.borderColor = "red";
    document.getElementById(
      "errorText"
    ).innerText = `Please enter correct price`;
    priceBox.style.display = "none";
  } else {
    document.getElementById("errorText").innerText = ``;
    priceBox.style.display = "block";
    priceSpan.innerHTML = `Текущая цена ${this.value}`;
    input.style.color = "green";
  }
};

deleteButton.addEventListener("click", () => {
  priceBox.style.display = "none";
  input.value = "";
});
