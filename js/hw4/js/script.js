"use strict";
const newUser = {
  getLogin: function () {
    return (newUser.login = (this.firstName[0] + this.lastName).toLowerCase());
  },
};

let firstName;
let lastName;

function createNewUser(firstName, lastName) {
  while (!firstName || !lastName) {
    firstName = prompt("Введите свое имя", firstName);
    lastName = prompt("Введите свою фамилию", lastName);
    if (firstName === null && lastName === null) {
      break;
    }
    newUser.firstName = firstName;
    newUser.lastName = lastName;
  }
  // return newUser;
}

createNewUser();

// newUser.getLogin();
// console.log(newUser);
console.log(newUser.getLogin());
