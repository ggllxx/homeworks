"use strict";
let array = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  ["Lviv", "Dniepr"],
];

function showArrElements(arr) {
  const div = document.createElement("div");
  document.body.prepend(div);
  div.className = "list";
  const ul = document.createElement("ul");
  div.prepend(ul);
  const li = document.createElement("li");

  arr.map((element) => {
    if (typeof element === "object") {
      ul.insertAdjacentHTML("beforeend", `<ul><li>${element}</li></ul>`);
    } else {
      ul.insertAdjacentHTML("beforeend", `<li>${element}</li>`);
    }
  });
}

showArrElements(array);
