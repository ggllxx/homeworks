"use strict";
const tabsHead = document.querySelector(".tabs-header");
const tabs = document.querySelectorAll(".tab");
const content = document.querySelectorAll(".tab-content");

function hideTabContent(arg) {
  for (let i = arg; i < content.length; i++) {
    content[i].classList.remove("show");
    content[i].classList.add("hide");
    tabs[i].classList.remove("active");
  }
}

hideTabContent();

function showTabContent(arg) {
  if (content[arg].classList.contains("hide")) {
    content[arg].classList.remove("hide");
    content[arg].classList.add("show");
    tabs[arg].classList.add("active");
  }
}

tabsHead.addEventListener("click", function (event) {
  let target = event.target;
  if (target && target.classList.contains("tab")) {
    for (let i = 0; i < tabs.length; i++) {
      if (target === tabs[i]) {
        hideTabContent(0);
        showTabContent(i);
        break;
      }
    }
  }
});
